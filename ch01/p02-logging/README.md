# Logging – the log crate

Sample usage of `log` crate with `env_logger` implementation.

## Run

To run project execute following command:
```shell script
cargo run
```

This will use default logging leve `ERROR`.

To specify different logging level use `RUST_LOG` environment variable:
```shell script
RUST_LOG=debug cargo run
```

This will change logging level to `DEBUG`, which is highest level and prints all logging statments.

Available logging levels:
 * `error`
 * `warn`
 * `info`
 * `debug`
 