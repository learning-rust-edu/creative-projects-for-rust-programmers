# Parsing the command line – the structopt crate

Sample usage of `structopt` crate.

## Run

When run without required command line arguments produces following error message:
```
error: The following required arguments were not provided:
    --result <result-file>

USAGE:
    p04-structopt [FLAGS] --result <result-file> [FILE]...

For more information try --help

```

Using required arguments like this:
```shell script
cargo run -- input1.txt input2.txt -v --result res.xyz
```

will produce following output:
```
Opt {
    verbose: true,
    result_file: "res.xyz",
    files: [
        "input1.txt",
        "input2.txt",
    ],
}
```
