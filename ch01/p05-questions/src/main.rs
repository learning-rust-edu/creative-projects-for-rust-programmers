use lazy_static::lazy_static;
use rand::{thread_rng, Rng};
#[macro_use]
extern crate log;

fn use_rand() {
    let mut rng = thread_rng();
    print!("random f32: ");
    for _ in 0..10 {
        let i: f32 = rng.gen_range(100.0, 400.0);
        print!("{} ", i);
    }
    println!();
    print!("random i32: ");
    for _ in 0..10 {
        let i: i32 = rng.gen_range(100, 400);
        print!("{} ", i);
    }
    println!();
}

lazy_static! {
    static ref SQUARES: Vec<i32> = {
        let mut v = Vec::new();
        for i in 1..200 {
            v.push(i * i);
        }
        v
    };
}

fn use_laze_static() {
    println!("Squared integers {:?}", *SQUARES);
}

fn use_log() {
    warn!("Warning message");
    info!("Info message");
}

use structopt::StructOpt;

#[derive(StructOpt, Debug)]
struct Opt {
    #[structopt(short = "l", long = "level")]
    level: i32,
}

fn use_args() {
    let opts = Opt::from_args();
    if opts.level < 0 || opts.level > 20 {
        error!("Level is out of range");
    } else {
        println!("level = {}", opts.level);
    }
}

fn main() {
    env_logger::init();
    use_rand();
    use_laze_static();
    use_log();
    use_args();
}
