use std::io::Write;

use actix_web::{App, HttpRequest, HttpResponse, HttpServer, Responder, web};

fn flush_stdout() {
    std::io::stdout().flush().unwrap();
}

async fn delete_file(info: web::Path<String>) -> impl Responder {
    let filename = &info.into_inner();
    println!("Deleting file \"{}\" ...", filename);
    flush_stdout();

    // actual delete
    println!("Deleted file \"{}\"", filename);
    HttpResponse::Ok()
}

async fn download_file(info: web::Path<String>) -> impl Responder {
    let filename = &info.into_inner();
    println!("Downloading file \"{}\" ...", filename);
    flush_stdout();

    // read the content of the file
    let content = "Content of the file.\n".to_string();

    println!("Downloaded file \"{}\"", filename);
    HttpResponse::Ok().content_type("text/plain").body(content)
}

async fn upload_specified_file(info: web::Path<String>) -> impl Responder {
    let filename = &info.into_inner();
    println!("Uploading file \"{}\" ...", filename);
    flush_stdout();

    // read the content of the file
    let _content = "Content of the file.\n".to_string();

    println!("Uploaded file \"{}\"", filename);
    HttpResponse::Ok()
}

async fn upload_new_file(info: web::Path<String>) -> impl Responder {
    let filename = &info.into_inner();
    println!("Uploading file \"{}*.txt\" ...", filename);
    flush_stdout();

    // read the content of the file
    let _content = "Content of the file.\n".to_string();
    let file_id = 17;
    let new_filename = format!("{}{}.txt", filename, file_id);

    println!("Uploaded file \"{}\"", new_filename);
    HttpResponse::Ok().content_type("text/plain").body(new_filename)
}

async fn invalid_resource(req: HttpRequest) -> impl Responder {
    println!("Invalid URI: \"{}\"", req.uri());
    HttpResponse::NotFound()
}

#[actix_web::main]
async fn main() -> std::io::Result<()> {
    let server_address = "127.0.0.1:8080";
    println!("Listening at address {} ...", server_address);
    HttpServer::new(|| {
        App::new()
            .service(
                web::resource("/{filename}")
                    .route(web::delete().to(delete_file))
                    .route(web::get().to(download_file))
                    .route(web::put().to(upload_specified_file))
                    .route(web::post().to(upload_new_file))
            )
            .default_service(web::route().to(invalid_resource))
    })
        .bind(server_address)?
        .run()
        .await
}
