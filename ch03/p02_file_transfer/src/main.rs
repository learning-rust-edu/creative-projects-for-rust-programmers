use futures_util::stream::StreamExt as _;
use std::fs::{File, OpenOptions};
use std::io::{ErrorKind, Write};
use actix_web::{App, HttpRequest, HttpResponse, HttpServer, Responder, web};
use rand::prelude::*;

fn flush_stdout() {
    std::io::stdout().flush().unwrap();
}

async fn delete_file(info: web::Path<String>) -> impl Responder {
    let filename = &info.into_inner();
    println!("Deleting file \"{}\" ...", filename);
    flush_stdout();

    // Delete the file
    match std::fs::remove_file(&filename) {
        Ok(_) => {
            println!("Deleted file \"{}\"", filename);
            HttpResponse::Ok()
        }
        Err(error) => {
            println!("Failed to deleted file \"{}\": {}", filename, error);
            HttpResponse::NotFound()
        }
    }
}

async fn download_file(info: web::Path<String>) -> impl Responder {
    let filename = &info.into_inner();
    println!("Downloading file \"{}\" ...", filename);
    flush_stdout();

    // read the content of the file
    fn read_file_content(filename: &str) -> std::io::Result<String> {
        use std::io::Read;
        let mut content = String::new();
        File::open(filename)?.read_to_string(&mut content)?;
        Ok(content)
    }

    match read_file_content(&filename) {
        Ok(content) => {
            println!("Downloaded file \"{}\"", filename);
            HttpResponse::Ok().content_type("text/plain").body(content)
        }
        Err(error) => {
            println!("Failed to read the file \"{}\": {}", filename, error);
            HttpResponse::NotFound().finish()
        }
    }
}

async fn upload_specified_file(
    mut payload: web::Payload,
    info: web::Path<String>
) -> impl Responder {
    let filename = info.into_inner().clone();
    println!("Uploading file \"{}\" ...", filename);
    flush_stdout();

    // read the content of the request
    let mut content = web::BytesMut::new();
    while let Some(item) = payload.next().await {
        content.extend_from_slice(&item.unwrap());
    }
        // Create the file
        let f = File::create(&filename);
        if f.is_err() {
            println!("Failed to create a file \"{}\"", filename);
            return HttpResponse::NotFound().into();
        }

        // Write the content into it
        if f.unwrap().write_all(&content).is_err() {
            println!("Failed to write a file \"{}\"", filename);
            return HttpResponse::NotFound().into();
        }
        println!("Uploaded file \"{}\" ...", filename);
        HttpResponse::Ok().finish()
}

fn open_file(filename_prefix: String) -> Result<(File, String), std::io::Error> {
    let mut rng = thread_rng();
    let mut attempts = 0;
    let mut file;
    let mut filename;
    const MAX_ATTEMPTS: u32 = 100;
    loop {
        attempts += 1;
        if attempts > MAX_ATTEMPTS {
            println!("Failed ot create new file with prefix \"{}\", after {} attempts.",
                     filename_prefix, MAX_ATTEMPTS);
            return Err(std::io::Error::new(ErrorKind::AddrNotAvailable, "Max attempts reached"));
        }
        // Generate a 3-digit pseudo random number
        filename = format!("{}{:03}.txt", filename_prefix, rng.gen_range(0..1000));

        // create a new file
        file = OpenOptions::new()
            .write(true)
            .create_new(true)
            .open(&filename);

        // if it was created, exit the loop
        if file.is_ok() {
            return Ok((file.unwrap(), filename));
        }
    }
}

async fn upload_new_file(
    mut payload: web::Payload,
    info: web::Path<String>,
) -> impl Responder {
    let filename_prefix = info.into_inner().clone();
    println!("Uploading file \"{}*.txt\" ...", filename_prefix);
    flush_stdout();

    let mut content = web::BytesMut::new();
    while let Some(item) = payload.next().await {
        content.extend_from_slice(&item.unwrap());
    }
    match open_file(filename_prefix) {
        Ok((mut file, filename)) => {
            if file.write_all(&content).is_err() {
                println!("Failed to write a file \"{}\"", filename);
                return HttpResponse::NotFound().into();
            }
            println!("Uploaded file \"{}\" ...", filename);
            HttpResponse::Ok().content_type("text/plain").body(filename)
        },
        Err(_) => HttpResponse::NotFound().finish()
    }
}

async fn invalid_resource(req: HttpRequest) -> impl Responder {
    println!("Invalid URI: \"{}\"", req.uri());
    HttpResponse::NotFound()
}

#[actix_web::main]
async fn main() -> std::io::Result<()> {
    let server_address = "127.0.0.1:8080";
    println!("Listening at address {} ...", server_address);
    HttpServer::new(|| {
        App::new()
            .service(
                web::resource("/{filename}")
                    .route(web::delete().to(delete_file))
                    .route(web::get().to(download_file))
                    .route(web::put().to(upload_specified_file))
                    .route(web::post().to(upload_new_file))
            )
            .default_service(web::route().to(invalid_resource))
    })
        .bind(server_address)?
        .run()
        .await
}
